import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from '../dashboard/dashboard.component';
import {SignInRedirectComponent} from './sign-in-redirect.component';


const routes: Routes = [
  {
    path: '',
    component: SignInRedirectComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SignInRedirectRoutingModule { }
