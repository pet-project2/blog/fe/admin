import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignInRedirectComponent } from './sign-in-redirect.component';
import {SignInRedirectRoutingModule} from './sign-in-redirect-routing.module';

@NgModule({
  declarations: [SignInRedirectComponent],
  imports: [
    SignInRedirectRoutingModule
  ]
})
export class SignInRedirectModule { }
