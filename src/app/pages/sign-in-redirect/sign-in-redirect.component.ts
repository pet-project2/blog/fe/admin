import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../shared/security/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-sign-in-redirect',
  templateUrl: './sign-in-redirect.component.html',
  styleUrls: ['./sign-in-redirect.component.css']
})
export class SignInRedirectComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.authService.completeLogin().then(user => {
      this.router.navigate(['/admin/dashboard'], { replaceUrl: true });
    });
  }

}
