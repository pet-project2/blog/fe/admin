import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../shared/security/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  user: any = {};
  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    // const accessToken = this.authService.accessToken;
    // console.log(accessToken);
  }

}
