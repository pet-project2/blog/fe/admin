import { NgModule } from '@angular/core';
import { StartComponent } from './start.component';
import {StartRoutingModule} from './start-routing.module';



@NgModule({
  declarations: [StartComponent],
  imports: [
    StartRoutingModule
  ]
})
export class StartModule { }
