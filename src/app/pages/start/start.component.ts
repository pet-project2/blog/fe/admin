import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../shared/security/auth.service';

@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.css']
})
export class StartComponent implements OnInit {

  constructor(private authService: AuthService) {

  }

  ngOnInit(): void {

  }

  onLogin(): void {
    this.authService.login();
  }
}
