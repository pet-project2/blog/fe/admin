import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignOutRedirectComponent } from './sign-out-redirect.component';

describe('SignOutRedirectComponent', () => {
  let component: SignOutRedirectComponent;
  let fixture: ComponentFixture<SignOutRedirectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignOutRedirectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignOutRedirectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
