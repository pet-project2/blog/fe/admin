import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../shared/security/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-sign-out-redirect',
  templateUrl: './sign-out-redirect.component.html',
  styleUrls: ['./sign-out-redirect.component.css']
})
export class SignOutRedirectComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.authService.completeLogout().then(response => {
      console.log(response);
      this.router.navigate(['/start'], { replaceUrl: true });
    });
  }

}
