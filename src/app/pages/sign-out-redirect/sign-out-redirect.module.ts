import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SignOutRedirectRoutingModule } from './sign-out-redirect-routing.module';
import {SignOutRedirectComponent} from './sign-out-redirect.component';


@NgModule({
  declarations: [SignOutRedirectComponent],
  imports: [
    CommonModule,
    SignOutRedirectRoutingModule
  ]
})
export class SignOutRedirectModule { }
