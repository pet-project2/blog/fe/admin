import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SignOutRedirectComponent} from './sign-out-redirect.component';


const routes: Routes = [
  {
    path: '',
    component: SignOutRedirectComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SignOutRedirectRoutingModule { }
