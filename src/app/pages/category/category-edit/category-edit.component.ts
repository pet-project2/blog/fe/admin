import { Component, OnInit } from '@angular/core';
import {Category} from '../models/category';

@Component({
  selector: 'app-category-edit',
  templateUrl: './category-edit.component.html',
  styleUrls: ['./category-edit.component.css']
})
export class CategoryEditComponent implements OnInit {
  category: Category;
  constructor() {
    this.category = new Category('1e8cfb2a-e068-4449-abc9-03237fcce619',
      '.NET On Way',
      '2021-05-18',
      true,
      'Tommy',
      'https://wallpapercave.com/wp/wp5156501.jpg');
  }

  ngOnInit(): void {
  }

}
