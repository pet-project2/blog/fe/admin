export class Category {
  constructor(public id: string,
              public name: string,
              public createOnDate: string,
              public status: boolean,
              public author: string,
              public image: string) {
  }
}
