import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CategoryRoutingModule} from './category-routing.module';
import { CategoryComponent } from './category.component';
import { CategoryListComponent } from './category-list/category-list.component';
import { CategoryHeaderComponent } from './category-header/category-header.component';
import { CategoryRowComponent } from './category-list/category-row/category-row.component';
import { CategoryEditComponent } from './category-edit/category-edit.component';


@NgModule({
  declarations: [CategoryComponent, CategoryListComponent, CategoryHeaderComponent, CategoryRowComponent, CategoryEditComponent],
    imports: [
        CategoryRoutingModule,
        CommonModule
    ]
})
export class CategoryModule { }
