import {Component, Input, OnInit} from '@angular/core';
import {Category} from '../../models/category';

@Component({
  // tslint:disable-next-line:component-selector
  selector: '[app-category-row]',
  templateUrl: './category-row.component.html',
  styleUrls: ['./category-row.component.css']
})
export class CategoryRowComponent implements OnInit {
  @Input() category: Category;
  constructor() { }

  ngOnInit(): void {
  }

}
