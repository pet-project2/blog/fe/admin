import { Component, OnInit } from '@angular/core';
import {Category} from './models/category';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  categories: Category[];
  constructor() {
    this.categories = [
      new Category('586a4024-fa76-43f2-a173-053bf070d288',
        '.NET On Way', '2021-05-18',
        true,
        'Tommy',
        'https://wallpapercave.com/wp/wp5156501.jpg'),
      new Category('cf2f8b5c-6554-4e65-89ea-aba7cee6f0c4',
        '.NET On Way',
        '2021-05-18', true,
        'Tommy',
        'https://wallpapercave.com/wp/wp5156501.jpg'),
      new Category('1e8cfb2a-e068-4449-abc9-03237fcce619',
        '.NET On Way',
        '2021-05-18',
        true,
        'Tommy',
        'https://wallpapercave.com/wp/wp5156501.jpg')
    ];
  }

  ngOnInit(): void {
  }

}
