import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {CategoryComponent} from './category.component';
import {CategoryEditComponent} from './category-edit/category-edit.component';

const routes: Routes = [
  {
    path: '',
    component: CategoryComponent
  },
  {
    path: ':id',
    component: CategoryEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoryRoutingModule { }
