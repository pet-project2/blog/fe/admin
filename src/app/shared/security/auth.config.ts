import {UserManagerSettings, WebStorageStateStore} from 'oidc-client';

export const authConfig: UserManagerSettings  = {
  authority: 'https://localhost:9001',
  client_id: 'blog.client.code',
  redirect_uri: 'http://localhost:4200/sign-in-callback',
  scope: 'openid api',
  response_type: 'code',
  post_logout_redirect_uri: 'http://localhost:4200/sign-out-callback',
  userStore: new WebStorageStateStore({ store: window.localStorage }),
  stateStore: new WebStorageStateStore( { store: window.localStorage}),
  loadUserInfo: true
};
