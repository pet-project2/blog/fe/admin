import { NgModule } from '@angular/core';
import {AuthService} from './auth.service';
import {authConfig} from './auth.config';
import {AuthGuard} from './auth.guard';


@NgModule({
  declarations: [],
  imports: [],
  providers: [AuthGuard, AuthService]
})
export class AuthenticationModule { }
