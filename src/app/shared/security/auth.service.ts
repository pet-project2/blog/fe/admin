import { Injectable } from '@angular/core';
import {SignoutResponse, User, UserManager} from 'oidc-client';
import {authConfig} from './auth.config';
import {BehaviorSubject, from, Observable, Subject} from 'rxjs';
import * as Oidc from 'oidc-client';
import {delay, map, take} from 'rxjs/operators';

@Injectable()
export class AuthService {
  private userManager: UserManager;
  public user: User;
  private loginChangedSubject = new Subject<boolean>();
  public loginChanged = this.loginChangedSubject.asObservable();

  constructor() {
    Oidc.Log.logger = console;
    Oidc.Log.level = Oidc.Log.DEBUG;
    this.userManager = new UserManager(authConfig);
  }

  login(): Promise<void> {
    return this.userManager.signinRedirect();
  }

  isLoggedIn(): Promise<boolean>{
    return this.userManager.getUser().then(user => {
      const isAuth = !!user && !user.expired;
      this.loginChangedSubject.next(isAuth);
      this.user = user;
      return isAuth;
    });
  }

  completeLogin(): Promise<User> {
    return this.userManager.signinRedirectCallback().then(user => {
      this.user = user;
      this.loginChangedSubject.next(!!user && !user.expired);
      return user;
    });
  }


  logout(): Promise<void> {
    return this.userManager.signoutRedirect();
  }

  completeLogout(): Promise<SignoutResponse> {
    this.user = null;
    this.loginChangedSubject.next(false);
    return this.userManager.signoutRedirectCallback();
  }

  getAccessToken(): Promise<string> {
    return this.userManager.getUser().then(user => {
      if (!!user && !user.expired) {
        return user.access_token;
      }
      else {
        return null;
      }
    });
  }
}
