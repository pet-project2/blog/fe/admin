import { NgModule } from '@angular/core';
import {FooterComponent} from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { LeftSidebarComponent } from './left-sidebar/left-sidebar.component';
import {RouterModule} from '@angular/router';



@NgModule({
  declarations: [FooterComponent, HeaderComponent, LeftSidebarComponent],
  exports: [
    HeaderComponent,
    LeftSidebarComponent,
    FooterComponent
  ],
  imports: [
    RouterModule
  ]
})
export class SharedModule { }
