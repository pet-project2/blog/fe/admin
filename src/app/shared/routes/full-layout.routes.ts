import {Routes} from '@angular/router';

export const FULL_LAYOUT_ROUTES: Routes = [
  {
    path: 'dashboard',
    loadChildren: () => import('../../pages/dashboard/dashboard.module').then(m => m.DashboardModule)
  },
  {
    path: 'category',
    loadChildren: () => import('../../pages/category/category.module').then(m => m.CategoryModule)
  },
  {
    path: 'unauthorized',
    loadChildren: () => import('../../pages/unauthorized/unauthorized.module').then(m => m.UnauthorizedModule)
  }
];
