import {Routes} from '@angular/router';

export const CONTENT_LAYOUT_ROUTES: Routes = [
  {
    path: 'start',
    loadChildren: () => import('../../pages/start/start.module').then(m => m.StartModule)
  },
  {
    path: 'sign-in-callback',
    loadChildren: () => import('../../pages/sign-in-redirect/sign-in-redirect.module').then(m => m.SignInRedirectModule)
  },
  {
    path: 'sign-out-callback',
    loadChildren: () => import('../../pages/sign-out-redirect/sign-out-redirect.module').then(m => m.SignOutRedirectModule)
  }
];
