import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FullLayoutComponent} from './layouts/full-layout/full-layout.component';
import {FULL_LAYOUT_ROUTES} from './shared/routes/full-layout.routes';
import {ContentLayoutComponent} from './layouts/content-layout/content-layout.component';
import {CONTENT_LAYOUT_ROUTES} from './shared/routes/content-layout.routes';
import {AuthGuard} from './shared/security/auth.guard';

const routes: Routes = [
  {
    path: '', redirectTo: 'start', pathMatch: 'full'
  },
  {
    path: '', component: ContentLayoutComponent, children: CONTENT_LAYOUT_ROUTES
  },
  {
    path: 'admin', component: FullLayoutComponent, children: FULL_LAYOUT_ROUTES
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
